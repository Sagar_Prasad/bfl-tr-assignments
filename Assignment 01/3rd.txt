
package calculator01;

import java.util.*;


class Operation {
    
    private int c;
    
    public void selections(int choice,int a,int b){
    switch(choice)
         {
             case 1:  add(a,b);
                 break;
                 
             case 2:  sub(a,b);
                 break; 
                 
             case 3:  mul(a,b);
                 break;
                 
             case 4:  div(a,b);
                 break;
                 
             default: 
                 break;    
         }
    }
    
    private void add(int a,int b){
        c=a+b;
        System.out.println("Addition Of two Number is : "+ c);
    }
        
    private void sub(int a,int b){
        c=a-b;
        System.out.println("Subtraction Of two Number is : "+ c);  
    }
    private void mul(int a,int b){
        c=a*b;
        System.out.println("Multiplication Of two Number is : "+ c);
    }   
    private void div(int a,int b){
        c=a/b;
        System.out.println("Division Of two Number is : "+ c);    
    
    }
}

class SquareRoot extends Operation{

 public static double squareRoot(int number) {
	double temp;

	double sr = number / 2;

	do {
		temp = sr;
		sr = (temp + (number / temp)) / 2;
	} while ((temp - sr) != 0);

	return sr;
    }
}



public class Calculator01  {

  
    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        int a,b,choice,num;
        
        SquareRoot obj=new SquareRoot();
        
        
         do{
         
        System.out.println("\n\n--------Enter Your Options --------\n\n");
        System.out.println("1.Addition \n2.Subtraction \n3.Multiplication \n4.Division \n5.Square Root\n6.Exist\n");
        
        choice =sc.nextInt();
        
        
         
         
         
         if(choice==5)
         {
           System.out.println("Enter a Number to Square Root of : ");
           num=sc.nextInt();
           System.out.println("Square root of "+ num+ " is: "+obj.squareRoot(num)+"\n");
           
         }
         
         else if(choice==6)
         {
            System.out.println("Thank You for using calculator");
            break;
         }
         else{
             System.out.println("Enter First Number : ");
             a =sc.nextInt();
             
             System.out.println("Enter Second Number : "); 
             b =sc.nextInt();
             
             obj.selections(choice,a,b);
         }
         
         }while(choice != 6);
         
           
         
         
         
        
    }
    
}
