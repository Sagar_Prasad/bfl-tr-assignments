
import java.util.Scanner;

abstract class Valid {

    public abstract void email_Validation(String string);

    public abstract void mobile_Validation(String string);

    public abstract void checkAge(int year);
}

class Main extends Valid {

    private static final String regex = "^(.+)@(.+)$";
    private static final String regex1 = "(0/91)?[7-9][0-9]{9}";

    public void validate(String email, String phone_number, int years) {
        email_Validation(email);
        mobile_Validation(phone_number);
        checkAge(years);
    }

    public void email_Validation(String email) {
        System.out.println("The Email you have entered is " + (email.matches(regex) ? "Valid." : "Invalid"));
    }

    public void mobile_Validation(String phone_number) {
        System.out.println("The Mobile Number is: " + (phone_number.matches(regex1) ? "Valid." : "Alert....Invalid"));
    }

    public void checkAge(int years) {

        if (2020 - years <= 18) {
            System.out.println("Your Age is less than 18 years");
        } else {
            System.out.println("You are 18+ year");
        }
    }

    public static void main(String[] args) {
        String emailID;
        String phoneNo;
        int year;
        Main obj = new Main();

        Scanner in = new Scanner(System.in);

        System.out.println("Enter Email ID : ");
        emailID = in.nextLine();

        System.out.println("Enter Mobile Number : ");
        phoneNo = in.nextLine();

        System.out.println("Enter Date of Birth Year(yyyy) : ");
        year = in.nextInt();

        obj.validate(emailID, phoneNo, year);
    }

}
